### Prerequisites

```
- Java 11 +
- Maven 3+
- Docker (Optional)

```

## EndPoints

- POST create customer:
http://localhost:8080/customer/

- GET customer:
http://localhost:8080/customer/[customerId]


### Installing

It can be run as a Docker container or as a Java Spring Boot application:

Using Docker: 

```
- unzip the folder
- cd [PATH]
- docker build -t test-service:1.0 .
- docker run -p 127.0.0.1:8080:8080 --name test-service -d test-service:1.0 
```

Using Java and Maven:
By Command line:

```
- unzip the folder
- cd [PATH]
- mvn clean install
- Run SpringTestBoot file 
```

Importing the project IDE

```
- unzip the folder
- Open the project (I used Intellij for this test)
- Run SpringTestBoot.
```

