package com.test.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.test.ServiceTestBoot;
import com.test.dto.Customer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = ServiceTestBoot.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerControllerTest {

    private MockMvc mockMvc;

    @Autowired
    CustomerController customerController;
    private ObjectMapper objectMapper;
    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(customerController)
                .build();
        objectMapper = getObjectMapper();
    }

    @Test
    public void create_customer_successfull() throws Exception {
        //Given
        Customer customer = Customer.builder().customerId(123).createdAt(LocalDate.now().minusDays(12)).build();

        //When
        MvcResult mvcResult = mockMvc.perform(post("/customer/").contentType(APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(customer)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customerId", is(notNullValue())))
                .andReturn();

        Customer response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Customer.class);

        //Then
        assertThat(response, is(notNullValue()));
        assertThat(response.getCustomerId(), is(123));
        assertThat(response.getExternalId(), is(notNullValue()));
        assertThat(response.getCreatedAt(), is(LocalDate.now().minusDays(12)));
    }

    @Test
    public void createCustomerWrongDate() throws Exception {
        //Given
        Customer customer = Customer.builder().customerId(123).createdAt(LocalDate.now().plusDays(12)).build();

        //Then
        ResultActions result = mockMvc.perform(post("/customer/").contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(customer)))
            .andExpect(status().is4xxClientError());

    }

    @Test
    public void createCustomerAndRetrieve() throws Exception {
        //Given
        Customer customer = Customer.builder().customerId(123).createdAt(LocalDate.now().minusDays(12)).build();

        //When
        MvcResult mvcResult = mockMvc.perform(post("/customer/").contentType(APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(customer)))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.customerId", is(notNullValue())))
          .andReturn();

        Customer response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Customer.class);

        mvcResult = mockMvc.perform(get("/customer/"+response.getCustomerId()))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.customerId", is(notNullValue())))
          .andReturn();

        response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Customer.class);

        //Then
        assertThat(response, is(notNullValue()));
        assertThat(response.getCustomerId(), is(123));
        assertThat(response.getExternalId(), is(notNullValue()));
        assertThat(response.getCreatedAt(), is(LocalDate.now().minusDays(12)));

    }

    @Test
    public void NotFoundCustomer() throws Exception {
        //Given
        String invalidCustomer = "123";
        //When
        mockMvc.perform(get("/customer/"+invalidCustomer))
          .andExpect(status().isNotFound())
          .andReturn();
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new ParameterNamesModule());
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper;
    }
}
