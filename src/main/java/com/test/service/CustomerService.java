package com.test.service;

import com.test.dto.Customer;
import com.test.exception.CustomerNotFoundException;
import com.test.exception.CustomerNotValidException;
import com.test.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.function.Function;

@Service
public class CustomerService {

    public final CustomerRepository customerRepository;

    private Function<Customer, Boolean> hasAValidCustomerDateCreation = anyCustomer ->
        anyCustomer.getCreatedAt().isAfter(LocalDate.now());

    @Autowired
    CustomerService(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    public Customer createCustomer(Customer customer) {
        if(hasAValidCustomerDateCreation.apply(customer)) {
            throw new CustomerNotValidException("Created date time needs to be in the past.");
        }
        return customerRepository.save(customer);
    }

    public Customer getCustomerById(int customerId) {
        return customerRepository
            .findById(customerId)
            .orElseThrow(
                () -> new CustomerNotFoundException("Customer not found for: "+ String.valueOf(customerId)));
    }
}
