package com.test;
public class Collatz {

    public static void main(String[] args) {
        tailCollatz(100);
        collatz(35);
        oneLineCollatz(5);
    }

    public static int tailCollatz(int n) {
        if(n < 1) return 0;
        else if(n == 1) return 1;
        else if (n % 2 == 0) return collatz(n / 2);
        else return collatz(3 * n + 1);
    }

    public static int collatz(int n) {
        if (n % 2 == 0) {
            return n / 2;
        }
        else {
            return (3 * n) + 1;
        }
    }

    public static int oneLineCollatz(int n) {
        return n % 2 == 0 ? collatz(n/2) : collatz(3 * n + 1);
    }
}
