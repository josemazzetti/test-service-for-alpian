package com.test.controller;

import com.test.dto.Customer;
import com.test.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CustomerController {

    public final CustomerService customerService;

    @Autowired
    CustomerController(CustomerService customerService){
        this.customerService = customerService;
    }

    @PostMapping({"/customer/"})
    @ResponseBody
    public ResponseEntity createCustomer(@RequestBody @Valid Customer customer) {
        return ResponseEntity
            .ok()
            .body(customerService.createCustomer(customer));
    }

    @GetMapping({ "/customer/{customerId}"})
    @ResponseBody
    public ResponseEntity getCustomer(@PathVariable(required = false, name = "customerId") Integer customerId) {
        return ResponseEntity
            .ok()
            .body(customerService.getCustomerById(customerId));
    }

}
