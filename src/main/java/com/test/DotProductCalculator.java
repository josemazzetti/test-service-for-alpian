package com.test;

import java.util.Arrays;
import java.util.stream.IntStream;

class DotProductCalculator {

  public static void main(String[] args ){
    System.out.println("result is" + calculateDotProductBySize(10));
  }

  public static int calculateDotProductBySize(int size) {

    int[] vector1 = new int[size];
    int[] vector2 = new int[size];

    Arrays.fill(vector1, 10);
    Arrays.fill(vector2, 20);

   return IntStream.range(0, vector1.length)
      .map( id -> vector1[id] * vector2[id])
      .reduce(0, Integer::sum);
  }
}

