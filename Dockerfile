FROM adoptopenjdk/openjdk11:alpine-jre
VOLUME /mnt
ADD /target/*.jar /mnt/test-service-latest.jar
RUN sh -c 'touch /mnt/test-service-latest.jar'
EXPOSE 8080:8080
ENTRYPOINT [ "sh", "-c", "java -Xmx512m -Xss512m -jar /mnt/test-service-latest.jar" ]